var express = require('express');
var router = express.Router();
var BicicletaController = require('../controllers/bicicleta');

router.get('/', BicicletaController.bicicletas_list);
router.get('/create', BicicletaController.bicicletas_create_get);
router.post('/create', BicicletaController.bicicletas_create_post);
router.get('/:id/update', BicicletaController.bicicletas_update_get);
router.post('/:id/update', BicicletaController.bicicletas_update_post);

router.post('/:id/delete', BicicletaController.bicicletas_delete_post);

module.exports = router;