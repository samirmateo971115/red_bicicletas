var express = require('express');
var router = express.Router();
var BicicletaController = require('../../controllers/api/bicicletaControllerAPI');
const Bicicletas = require('../../models/bicicletas');

router.get('/', BicicletaController.bicicleta_list);
router.post('/create', BicicletaController.bicicleta_create);
router.post('/update', BicicletaController.bicicleta_update);
router.delete('/:id/delete', BicicletaController.bicicleta_delete);


module.exports = router;