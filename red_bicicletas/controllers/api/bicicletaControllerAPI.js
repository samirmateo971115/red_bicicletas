var Bicicleta = require('../../models/bicicletas');
const Bicicletas = require('../../models/bicicletas');
const { bicicletas_create_get } = require('../bicicleta');


exports.bicicleta_list = function(req, res){
    res.status(200).json({
        bicicletas: Bicicleta.allbicis
    });
}

exports.bicicleta_create = function(req, res){
    var bici = new Bicicletas(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicletas.add(bici);

    res.status(200).json({
        bicicletas:bici

    });  
}

exports.bicicleta_update = function (req,res) {
    var bici = Bicicletas.findByID(req.body.id);
    
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [ req.body.lat, req.body.lng];

    res.status(200).json({
        bicicleta: bici
    });
}



exports.bicicleta_delete = function (req, res){
    Bicicletas.findByID(req.body.id);

    res.status(204).send();
}