var Bicicletas= function (id, color, modelo, ubicacion) {
    this.id= id;
    this.color= color;
    this.modelo= modelo;
    this.ubicacion= ubicacion;

    
}

Bicicletas.prototype.toString = function () {
    return "id" + this.id + "/ color: " + this.color;
    
}

Bicicletas.allbicis = [];

Bicicletas.add = function (aBici) {
    Bicicletas.allbicis.push (aBici);
    
}

Bicicletas.findByID = function(aBiciID){
    var aBici = Bicicletas.allbicis.find(x => x.id == aBiciID);
    if (aBici){
        return aBici;
    }
    else
     throw new error(`No existe una bicicleta con este ID ${aBiciID}`);
}

Bicicletas.removeID = function(aBiciID){
    for(var i=0; i< Bicicletas.allbicis.length; i++){
        if(Bicicletas.allbicis[i].id == aBiciID){
            Bicicletas.allbicis.splice(i, 1);
         break;

        }
        
    }


}

var a = new Bicicletas(1, 'rojo', 'urbana',[4.794751, -75.692292] );
var b = new Bicicletas(2, 'blanca', 'urbana',[4.795071, -75.688396] );

Bicicletas.add(a);
Bicicletas.add(b);

module.exports= Bicicletas;